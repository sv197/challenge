//
//  ViewController.swift
//  Challenge
//
//  Created by The App Experts on 05/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    
    enum segueName: String {
        case details
    }
    
    
    @IBOutlet var tableView: UITableView!
    let viewModel = ViewModel()
    var accountData = RootObject()
    var selectedAccount = DataModel()
    var configurator: AccountConfigurator!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurator = AccountConfigurator(viewModel: viewModel)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        viewModel.getAccountDataFromNetworkManager { (accounts) in
            DispatchQueue.main.async {
                self.accountData = accounts
                self.tableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "details" {
//            if let nextViewController = segue.destination as? DetailViewController {
//                nextViewController.selectedAccount = selectedAccount
//            }
            configurator.configureAccount(segue: segue, parameters: selectedAccount)
//            configurator.route(to: segueName.details.rawValue, from: self, parameters: selectedAccount)
        }
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        accountData.numberOfRowsInSection()
        //        viewModel.networkManager.model.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        guard let account = accountData.item(at: indexPath) else {
            return cell
        }
        cell.textLabel?.text = account.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let account = accountData.item(at: indexPath) else {
            return
        }
        selectedAccount = account
        performSegue(withIdentifier: "details", sender: self)
        
    }
}
