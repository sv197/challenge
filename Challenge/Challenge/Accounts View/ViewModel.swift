//
//  ViewModel.swift
//  Challenge
//
//  Created by The App Experts on 05/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

class ViewModel {
    
    let networkManager = NetworkManager()
    
//    func getTextFromNetworkManager(completion: @escaping ((DataModel) -> ())) {
//        networkManager.setText() { (message) in
//            print(message.text)
//            completion(message)
//        }
//    }
    
    func getAccountDataFromNetworkManager(completion: @escaping ((RootObject) -> ())) {
        networkManager.setAccountData() { (accounts) in
            print(accounts)
            completion(accounts)
        }
    }
    
}
