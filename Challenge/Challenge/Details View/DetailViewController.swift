//
//  DetailViewController.swift
//  Challenge
//
//  Created by The App Experts on 09/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var viewModel: DetailViewModel!
    @IBOutlet var accountTypeLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var accountNumberLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var balanceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel.setAccountData { (account) in
            self.accountTypeLabel?.text? += account.kind
            self.titleLabel?.text? += account.title
            self.accountNumberLabel?.text? += account.number
            self.currencyLabel?.text? += account.currency
            self.balanceLabel?.text? += String(account.balance)
        }
    }

}
