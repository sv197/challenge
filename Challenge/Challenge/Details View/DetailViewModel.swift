//
//  DetailViewModel.swift
//  Challenge
//
//  Created by The App Experts on 14/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

class DetailViewModel {

    var selectedAccount: DataModel!
    
    init(selectedAccount: DataModel) {
        self.selectedAccount = selectedAccount
    }
    
    func setAccountData(completion: @escaping ((DataModel)->())) {
        completion(selectedAccount)
    }

}
