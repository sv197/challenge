//
//  DataModel.swift
//  Challenge
//
//  Created by The App Experts on 05/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

struct RootObject: Codable {
    var accounts: [DataModel] = []
}

struct DataModel:Codable {
    
    //    var text: String = ""
    var kind: String = ""
    var title: String = ""
    var number: String = ""
    var balance: Double = 0
    var currency: String = ""
    
}

extension RootObject {
    var numberOfSections: Int {
        return 1 //return sections
    }
    
    func numberOfRowsInSection() -> Int {
        return accounts.count //use section too if needed
    }
    
    func item(at indexPath: IndexPath) -> DataModel? {
        if indexPath.section < 0 || indexPath.section > numberOfSections {
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection() {
            return nil
        }
        
        return accounts[indexPath.row]
    }
}

