//
//  Router.swift
//  Challenge
//
//  Created by The App Experts on 12/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import UIKit

//protocol Router {
////   func route(to routeID: String, from context: UIViewController, parameters: Any?)
//}

class AccountConfigurator {
    
    unowned var viewModel: ViewModel!
    
    init(viewModel: ViewModel) {
        self.viewModel = viewModel
    }
    
    func configureAccount(segue:UIStoryboardSegue, parameters: Any?) {
        guard let selectedAccount = parameters as? DataModel else {
            return
        }
        if let nextViewController = segue.destination as? DetailViewController {
//            nextViewController.selectedAccount = selectedAccount
            let vm = DetailViewModel(selectedAccount: selectedAccount)
            nextViewController.viewModel = vm
        }
    }
    
//    func route(to routeID: String, from context: UIViewController, parameters: Any?) {
//        guard let route = ViewController.segueName(rawValue: routeID) else {
//            return
//        }
//        switch route {
//        case .details:
//            let vc = DetailViewController()
//            if let selectedAccount = parameters as? DataModel  {
////                vc.selectedAccount = selectedAccount
//                let vm = DetailViewModel(selectedAccount: selectedAccount)
//                vc.viewModel = vm
////                context.performSegue(withIdentifier: routeID, sender: context)
//            }
//
////            context.performSegue(withIdentifier: routeID, sender: context)
////            context.navigationController?.pushViewController(vc, animated: true)
//        }
//    }
    
    
}
