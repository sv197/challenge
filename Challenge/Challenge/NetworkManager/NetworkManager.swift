//
//  NetworkManager.swift
//  Challenge
//
//  Created by The App Experts on 06/10/2020.
//  Copyright © 2020 The App Experts. All rights reserved.
//

import Foundation

class NetworkManager {
    var model = RootObject()
    
//        func setText(completion: @escaping ((DataModel) -> ())) {
//            self.model.text = "Hello World"
//            completion(model)
//        }
    
    
    func setAccountData(completion: @escaping ((RootObject) -> ())) {
        let urlString = "https://my-json-server.typicode.com/shanev96/challenge/db"
        loadJsonFrom(urlString: urlString) { (result) in
            switch (result) {
            case .success(let data):
                self.parseJson(jsonData: data)
                completion(self.model)
            case .failure(let error):
                print(error)
            }
        }
        //        parseJson(jsonData: jsonData)
    }
    
//    func setAccountDetails(completion: @escaping ((DataModel)->())) {
//        
//    }
    
    
    private func readJsonFile(forName name: String) -> Data? {
        do {
            if let path = Bundle.main.path(forResource: name,
                                           ofType: "json"),
                let jsonData = try String(contentsOfFile: path).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }
    
    private func loadJsonFrom(urlString: String, completion: @escaping (Result<Data, Error>) -> Void) {
        if let url = URL(string: urlString) {
            let urlSession = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                }
                
                if let data = data {
                    completion(.success(data))
                }
            }
            
            urlSession.resume()
        }
    }
    
    private func parseJson(jsonData: Data) {
        do {
            let decodedData = try JSONDecoder().decode(RootObject.self,
                                                       from: jsonData)
            self.model = decodedData
        } catch {
            print("decode error")
        }
    }
}

//func getJSONData(_ sourceID: String, completion: @escaping () -> Void) {
//    //create session
//    let defaultSession = URLSession(configuration: .default)
//
//    //define URLComponents
//    // Create the URL Components
//    var urlComps = URLComponents()
//    urlComps.scheme = "https"
//    urlComps.host = "newsapi.org"
//    urlComps.path = "/v2/top-headlines"
//
//    urlComps.queryItems = [URLQueryItem(name: "sources", value: sourceID),
//                           URLQueryItem(name: "apiKey", value: "5f28b615f1ab4ff3bdd7a9b61fe6c6fe")]
//    //Get the URL from the URL Components
//    guard let url = urlComps.url else {
//        print("Unable to create URL")
//        return
//    }
//    //send request for data
//    let dataTask = defaultSession.dataTask(with: url) { (data, response, error) in
//
//        if let error = error {
//            print("Error - \(error.localizedDescription)")
//        } else if let response = response as? HTTPURLResponse {
//
//            switch response.statusCode {
//            case 200...299:
//                if let data = data {
//                    //initialise model to remove any pre-existing data
//                    self.<#arrayName#> = []
//                    //parse data
//                    self.parseJSONData(data)
//                }
//            default: print("Found \(response.statusCode)")
//            }
//        }
//        //reload table
//        completion()
//    }
//    //start task
//    dataTask.resume()
//}
